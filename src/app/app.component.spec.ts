import { TestBed, async, ComponentFixture, fakeAsync } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { of } from "rxjs";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

describe("AppComponent", () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let mockWeatherApi, apiStatsSpy;
  let httpMock: HttpTestingController;

  mockWeatherApi = of({
    name: "Chicago",
    wind: 22,
    temp: 15
  });

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        apiStatsSpy = spyOn(component, "setApiStats").and.callThrough();
        fixture.detectChanges();
      });
    httpMock = TestBed.get(HttpTestingController);
  }));

  it("should create the app", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    const requests = [];
    component.cities.forEach(city => {
      requests.push(
        httpMock.expectOne({
          url: city,
          method: "GET"
        })
      );
    });

    requests.forEach(request => {
      request.flush({});
    });
  });

  it("should call the setApiStats function on init", () => {
    expect(apiStatsSpy).toHaveBeenCalled();
    const requests = [];
    component.cities.forEach(city => {
      requests.push(
        httpMock.expectOne({
          url: city,
          method: "GET"
        })
      );
    });

    requests.forEach(request => {
      request.flush({});
    });
  });

  it("should get weather data from api for every city", fakeAsync(() => {
    const requests = [];
    component.cities.forEach(city => {
      requests.push(
        httpMock.expectOne({
          url: city,
          method: "GET"
        })
      );
    });

    requests.forEach(request => {
      request.flush({});
    });
  }));

  it("should populate weather data from the api call", () => {
    const requests = [];
    component.cities.forEach(city => {
      requests.push(
        httpMock.expectOne({
          url: city,
          method: "GET"
        })
      );
    });

    requests.forEach(request => {
      request.flush({});
    });
    expect(component.weatherData.length).toEqual(component.cities.length);

  });

  afterEach(() => {
    httpMock.verify();
  });

});