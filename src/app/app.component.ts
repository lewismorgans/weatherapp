import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CitiesWeather } from "./models/cities-list.model.component";
import { forkJoin, Observable, from } from "rxjs";
import { take, map } from "rxjs/operators";
import { WeatherModel } from "./models/weather-data.model.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})

export class AppComponent implements OnInit {

  public weatherData = new Array<WeatherModel>();
  public readonly cities = [
    CitiesWeather.Chicago,
    CitiesWeather.Hawaii,
    CitiesWeather.London,
    CitiesWeather.Tokyo,
    CitiesWeather.Bali
  ]

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.setApiStats();
  }

  public getApiStats(): Observable<any> {
    const responses = this.cities
      .map(url => this.http.get(url));

    return forkJoin(responses);
  }

  public setApiStats(): void {
    this.getApiStats()
      .subscribe(resp => {
        resp.forEach(element => {
          this.weatherData.push(element);
        });
      });
  }
}