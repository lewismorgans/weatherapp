export enum CitiesWeather {
    London = 'https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6f7128a36941a41d180aa5c39248384&units=metric',
    Hawaii = 'https://api.openweathermap.org/data/2.5/weather?q=hawaii,us&appid=b6f7128a36941a41d180aa5c39248384&units=metric',
    Bali = 'https://api.openweathermap.org/data/2.5/weather?q=bali,cm&appid=b6f7128a36941a41d180aa5c39248384&units=metric',
    Tokyo = 'https://api.openweathermap.org/data/2.5/weather?q=tokyo,jp&appid=b6f7128a36941a41d180aa5c39248384&units=metric',
    Chicago = 'https://api.openweathermap.org/data/2.5/weather?q=chicago,us&appid=b6f7128a36941a41d180aa5c39248384&units=metric'
}