export interface WeatherModel {
    name: string,
    temperature: number,
    wind: number
}