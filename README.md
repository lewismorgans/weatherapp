# WeatherData

## App.Component.TS

Inside the class component itself I started by creating two arrays to contain the relevant information needed to handle and display the weather data to the end user. Utilising an exported enum in the models folder I was able to create a clean minimalistic array (cities) to hold the API end points that I'm going to be leveraging. 

I've created two main functions to handle the logic driving the class, getApiStats and setApiStats. Both are fully unit tested using Angular's built in tester Jasmine.

## getApiStats

Declaring a container variable I'm able to loop through my array by using the map operator, running the HTTP get method and returning the object value from the API, storing it into the variable. This is a clean method of handling the data, in that no matter the size of the original array that it’s mapping, the code length and implementation stays the same. If the list were to change to 10 or 20 cities this function would not be affected, thus avoiding bugs, errors and developer time.

Once all data responses have been completed, the forkJoin operator has been used to join the responses together and return them for use in the setApiStats function, talked about below.

## setApiStats

The setApiStats function calls upon the getApiStats function, which is returning an array of observable data that I'm able to subscribe to. Subscribing to the objects returned I'm able to push these into an array to contain all weather data. Consideration has been taken as to whether this could be done in one function, avoiding a function call altogether, but from a design perspective and personal experience it would suit best practices to separate functionality.

## TemplateView

The data is displayed using the Angular native operator *ngFor. Looping through the array I'm able to display the specific data required, such as wind, temperature and location, to the end user. I've chosen this method to keep the class as light as possible; due to the data never actually being used for any calculations or operations elsewhere in the class or application, it made sense not to use switches or maps here.

# Conclusions

I believe this implementation of the requested test meets the requirements and follows strong TypeScript and latest JavaScript functionality, with consideration given towards function automation as mentioned above to avoid errors in the future if mutated. The CSS used clearly displays the requested information, and all unit tests have been written cleanly with conclusions to be able to refactor if worked with other developers.
